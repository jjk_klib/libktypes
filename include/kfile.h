/**
 * @file kfile.h
 * @author Jason Kushmaul
 * @date 9 Sep 2017
 * @brief Easy funcs for fs
 *
 * I usually only care about the path I have, and a yes or no.@@.
 * @see https://klib.gitlab.io/libktypes/
 */
#ifndef KFILE_H
#define KFILE_H
#include <stdint.h>
#include <stddef.h>

/**
 * @brief write the entire buffer to a file in 4k chunks
 *
 * @param fpath p_fpath:...
 * @param buffer p_buffer:...
 * @param len p_len:...
 * @return int | 0 on success, -1 on error
 */
int kfile_write_all(const char *fpath, const uint8_t *buffer, const size_t len);

/**
 * @brief gets the first directory in path
 *
 * @param path p_path:...
 * @return char* | returns first directory in the path or NULL if path is empty
 */
char *kfile_top_dir_allocs(const char *path);

/**
 * @brief Returns ptr to next directory in path.
 *
 * @param path p_path:...
 * @return const char* | ptr to next path part, NULL if no more directories exist or only one part of path exists
 */
const char *kfile_next_dir(const char *path);

/**
 * @brief Checks if specifically, a dir exists.
 *
 * @param path p_path:...
 * @return int | 0 on is dir and exists, non-zero on error or not a dir
 */
int kfile_dir_exists(const char *path);

/**
 * @brief will quetly create a single dir
 *
 * @param path p_path:...
 * @return int | 0 on success or exists, nonzero on error
 */
int kfile_mkdir(const char *path);

/**
 * @brief Queitly & Recusively create given path
 *
 * @param base_path p_base_path:...
 * @param path p_path:...
 * @return int | 0 on success or exists, non-zero on error
 */
int kfile_mkdir_recurse(char *base_path, char *path);

/**
 * @brief Queitly & Recursively traverse path, delete leaves
 *
 * @param path p_path:...
 * @return int | 0 on success, non zero on error
 */
int kfile_rmdir_recurse(char *path);

/**
 * @brief Checks if given path is a directory
 *
 * @param path p_path:...
 * @return int | 0 on is dir, -1 on error or non-dir
 */
int kfile_is_dir(const char *path);

/**
 * @brief try to move a file without overwriting
 * glibc does not support this yet.
 *
 * @param src p_src:...
 * @param dst p_dst:...
 * @return int | 0 on success, -1 on exists, non zero on error.
 */
int kfile_move_noreplace(const char *src, const char *dst);

/**
 * @brief check if given path exists.  Does not follow normal error
 *
 * @param path p_path:...
 * @return int | 0 on does not exist, 1 on exists, -1 on error
 */
int kfile_path_exists(const char *path);

#endif
