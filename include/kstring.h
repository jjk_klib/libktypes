/**
 * @file kstring.h
 * @author Jason Kushmaul
 * @date 9 Sep 2017
 * @brief Easy funcs for fs
 *
 * Just wrapping string functions@@.
 * @see https://klib.gitlab.io/libktypes/
 */
#ifndef KSTRING_H
#define KSTRING_H
#include <stddef.h>

/**
 * @brief Calculates length then allocates and snprintf
 *
 * @param ptr p_ptr: destination, will be allocated
 * @param fmt p_fmt: standard format and arguments to follow
 * @return long unsigned int | size of new ptr, or 0 on error
 */
size_t kstring_sprintf_allocs(char **ptr, const char *fmt, ...);

/**
 * @brief Checks to see if a char value is inc_between two values
 *
 * @param left p_left:...
 * @param check p_check:...
 * @param right p_right:...
 * @return int | 0 on success, -1 on not found
 */
int kstring_char_in_range(char left, char check, char right);


/**
 * @brief Used to remove delimeters
 *
 * @param line p_line:...
 * @param delim p_delim:...
 * @return char*
 */
char *kstring_ltrim(char *line, const char *delim);

/**
 * @brief Looks for another delim in line
 *
 * @param line p_line:...
 * @param delim p_delim:...
 * @return char* | ptr further into line where delimeter appears or NULL
 */
char *kstring_next_token_allocs(char **line, const char *delim);

/**
 * @brief Given line and delim, will allocate new argv on return and populate count.
 *
 * Empty values are excluded
 *
 * @param line p_line:...
 * @param delim p_delim:...
 * @param count p_count:...
 * @return char** | allocated array with allocated char* each
 */
char **kstring_split_allocs(char *line, const char *delim, int *count);

/**
 * @brief Given line and delim, counts how many fields will occur
 *
 * @param line p_line:...
 * @param delim p_delim:...
 * @return int
 */
int kstring_split_count(char *line, const char *delim);

#endif
