#ifndef KSORTEDLIST_H
#define KSORTEDLIST_H

#include <klist.h>
#include <stddef.h>


typedef struct ksortedlist_t {
	klist_t list;
	klist_comparison_f comparator;
} ksortedlist_t;

void ksortedlist_init(ksortedlist_t *list, klist_comparison_f comparator);
klist_node_t *ksortedlist_add_allocs(ksortedlist_t *list, void *value);


#endif
