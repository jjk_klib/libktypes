#include <kstring.h>
#include <string.h>
#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>

size_t kstring_sprintf_allocs(char **ptr, const char *fmt, ...)
{
	va_list args;

	va_start(args, fmt);
	size_t size = vsnprintf(NULL, 0, fmt, args);
	va_end(args);
	if (size == (size_t)-1) {
		return (size_t)-1;
	}

	va_start(args, fmt);
	*ptr = malloc(size + 1);
	vsnprintf(*ptr, size + 1, fmt, args);
	va_end(args);

	return size;
}

int kstring_char_in_range(char left, char check, char right)
{
	int result = -1;

	if ( check >= left && check <= right) {
		result = 0;
	}
	return result;
}


char *kstring_ltrim(char *line, const char *delim)
{
	int line_len = strlen(line);
	int trim_len = strspn(line, delim);

	if (trim_len == line_len) {
		return 0;
	}
	line += trim_len;
	return line;
}

char *kstring_next_token_allocs(char **line, const char *delim)
{
	int line_len = strlen(*line);

	if (line_len == 0) {
		return NULL;
	}
	int tok_len = strcspn(*line, delim);

	char *retval = (char*)malloc(tok_len + 1);

	memset(retval, 0, tok_len + 1);
	strncpy(retval, *line, tok_len);

	*line += tok_len + 1;
	return retval;
}

int kstring_split_count(char *line, const char *delim)
{
	int count = 1;
	int delimiting = 0;

	for (int i = 0; i < strlen(line); i++) {
		if (strchr(delim, line[i])) {
			if (!delimiting) {
				delimiting = 1;
				count++;
			}
		} else {
			delimiting = 0;
		}
	}
	return count;
}


char **kstring_split_allocs(char *line, const char *delim, int *count)
{
	size_t tmp_count = kstring_split_count(line, delim);
	char **results = malloc(sizeof(char*) * tmp_count);

	memset(results, 0, sizeof(char*) * tmp_count);
	int i = 0;
	while (line != 0 && i < tmp_count) {
		line = kstring_ltrim(line, delim);
		if (line == 0) {
			break;
		}
		results[i++] = kstring_next_token_allocs(&line, delim);
	}
	*count = i;
	return results;
}
