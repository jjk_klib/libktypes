#include <klist.h>
#include <stdlib.h>
#include <string.h>

klist_node_t* klist_node_new_allocs(void *value)
{
	klist_node_t* new = calloc(KLIST_NODE_T_SIZE, 1);

	new->value = value;
	return new;
}

void klist_node_release_frees(klist_node_t *list_node)
{
	free(list_node);
}

void klist_init(klist_t *list)
{
	memset(list, 0, KLIST_T_SIZE);
}

void klist_release_frees(klist_t* list)
{
	while (list->head) {
		klist_node_t *node = list->head;
		klist_remove_node(list, node);
		klist_node_release_frees(node);
	}
}

klist_node_t *klist_find_node_by_value_address(klist_t *list, void *ptr)
{
	if (list->head == 0) {
		return 0;
	}
	KLIST_FOR_EACH_NODE(node, list){
		if (node->value == ptr) {
			return node;
		}
	}
	return 0;
}

klist_node_t *klist_find_node_by_comparison(klist_t*list, void* needle, klist_comparison_f comparator)
{
	if (list->head == 0) {
		return 0;
	}
	KLIST_FOR_EACH_NODE(node, list) {
		if (comparator(needle, node->value) == 0) {
			return node;
		}
	}
	return 0;
}

void klist_remove_node(klist_t *list, klist_node_t *list_node)
{
	if (list->head != NULL) {
		list->count--;
	}

	if (list_node->prev) {
		list_node->prev->next = list_node->next;
	}
	if (list_node->next) {
		list_node->next->prev = list_node->prev;
	}
	if (list->head == list_node) {
		list->head = list_node->next;
	}
	if (list->tail == list_node) {
		list->tail = list_node->prev;
	}
	list_node->prev = NULL;
	list_node->next = NULL;

}

//if before == NULL, simply appends
//if before != NULL, inserts to_insert before that node
void klist_insert_node(klist_t *list, klist_node_t *before, klist_node_t *to_insert)
{
	if (before == NULL) {
		if (list->tail == NULL) {
			list->head = to_insert;
			list->tail = to_insert;
		} else {
			list->tail->next = to_insert;
			to_insert->prev = list->tail;
			list->tail = to_insert;
		}
	} else {
		to_insert->next = before;
		to_insert->prev = before->prev;
		before->prev = to_insert;

		if (to_insert->prev == NULL ) {
			list->head = to_insert;
		} else{
			to_insert->prev->next = to_insert;
		}
	}
	list->count++;
}

klist_node_t *klist_insert_value_allocs(klist_t *list, klist_node_t *before, void *ptr)
{
	klist_node_t *node = klist_node_new_allocs(ptr);

	klist_insert_node(list, before, node);
	return node;
}

