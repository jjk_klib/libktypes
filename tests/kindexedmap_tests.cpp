extern "C" {
#include <string.h>
}
#include <gtest/gtest.h>
#include <gmock/gmock.h>

using namespace testing;
#include <atomic>
using namespace std;

extern "C" {
    #include <string.h>
    #include <kindexedmap.h>
    #include <string.h>
}


TEST(kindexedmap, kindexedmap_init_allocs) {
	kindexedmap_t imap = {};

	kindexedmap_init_allocs(&imap, NULL, 1000);
	kindexedmap_release_frees(&imap);
}

int voidstrcmp(void *left, void *right)
{
	if (!( left || right)) {
		if (left) {
			return -1;
		} else if (right) {
			return 1;
		}
	}

	return strcmp((char*)left,  (char*)right);
}
TEST(kindexedmap, kindexedmap_put_allocs) {
	kindexedmap_t imap = {};

	kindexedmap_init_allocs(&imap, voidstrcmp, 1000);
	khashmap_key_t key = { 0 };
	key.data = (uint8_t*)"test";
	key.len = strlen((char*)key.data);

	void *result;


	void *value = (char*)"value";
	result = kindexedmap_put_allocs(&imap, &key, value);
	ASSERT_EQ(result, (void*)NULL);
	result = kindexedmap_get(&imap, &key);
	ASSERT_NE(result, (void*)NULL);
	ASSERT_EQ(imap.index.list.count, 1);
	ASSERT_EQ(imap.map->total_entries, 1);
	ASSERT_EQ(result, value);

	khashmap_key_t key2 = { 0 };
	key2.data = (uint8_t*)"tesu";
	key2.len = strlen((char*)key2.data);
	void *value2 = (void*)"value2";
	int intresult = voidstrcmp(value, value2);
	ASSERT_LT(intresult, 0);
	result = kindexedmap_put_allocs(&imap, &key2, value2);
	ASSERT_EQ((void*)NULL, result);
	result = kindexedmap_get(&imap, &key2);
	ASSERT_NE((void*)NULL, result);
	ASSERT_EQ(imap.index.list.count, 2);
	ASSERT_EQ(imap.map->total_entries, 2);
	ASSERT_EQ(imap.index.list.head->value, value);
	ASSERT_EQ(imap.index.list.tail->value, value2);



	kindexedmap_release_frees(&imap);
}

TEST(kindexedmap, kindexedmap_get_allocs) {

	kindexedmap_t imap = {};

	kindexedmap_init_allocs(&imap, voidstrcmp, 1000);
	khashmap_key_t key = { 0 };
	key.data = (uint8_t*)"test";
	key.len = strlen((char*)key.data);

	void *result;

	void *value = (char*)"value";
	kindexedmap_put_allocs(&imap, &key, value);
	result = kindexedmap_get(&imap, &key);
	ASSERT_EQ(value, result);
	khashmap_key_t key2 = { 0 };
	key2.data = (uint8_t*)"tesu";
	key2.len = strlen((char*)key2.data);
	void *value2 = (void*)"value2";
	result = kindexedmap_put_allocs(&imap, &key2, value2);
	ASSERT_EQ((void*)NULL, result);
	result = kindexedmap_get(&imap, &key2);
	ASSERT_NE((void*)NULL, result);
	ASSERT_EQ(2, imap.index.list.count);
	ASSERT_EQ(2, imap.map->total_entries);
	ASSERT_NE(imap.index.list.head->value, imap.index.list.tail->value);
	ASSERT_EQ(value, imap.index.list.head->value);
	ASSERT_EQ(value2, imap.index.list.tail->value);



	kindexedmap_release_frees(&imap);
}

TEST(kindexedmap, kindexedmap_remove_frees){
	kindexedmap_t imap = {};

	kindexedmap_init_allocs(&imap, voidstrcmp, 1000);
	khashmap_key_t key = { 0 };
	key.data = (uint8_t*)"test";
	key.len = strlen((char*)key.data);

	void *result;

	void *value = (char*)"value";
	kindexedmap_put_allocs(&imap, &key, value);
	result = kindexedmap_remove_frees(&imap, &key);
	ASSERT_EQ(value, result);
	ASSERT_EQ(0, imap.map->total_entries);
	ASSERT_EQ(0, imap.index.list.count);



	kindexedmap_release_frees(&imap);
}

TEST(kindexedmap, kindexedmap_release_frees_initialized){
	kindexedmap_t imap = {};

	kindexedmap_init_allocs(&imap, voidstrcmp, 1000);
	kindexedmap_release_frees(&imap);
}
TEST(kindexedmap, kindexedmap_release_frees_zeroed){
	kindexedmap_t imap;

	bzero(&imap, sizeof(kindexedmap_t));
	kindexedmap_init_allocs(&imap, voidstrcmp, 1000);
	kindexedmap_release_frees(&imap);
}
