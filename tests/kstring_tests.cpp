#include <gtest/gtest.h>
#include <gmock/gmock.h>

using namespace testing;


extern "C" {
    #include <kstring.h>
}

TEST(kstring_ltrim, works){
	char *actual = 0;
	const char *delim = " ";
	char *leftwithspaces = strdup("   test  ");

	actual = kstring_ltrim(leftwithspaces, delim);
	ASSERT_TRUE(!strcmp("test  ", actual));

	char *nospaces = strdup("test");
	actual = kstring_ltrim(nospaces, delim);
	ASSERT_TRUE(!strcmp("test", actual));

	char *allspaces = strdup("   ");
	actual = kstring_ltrim(allspaces, delim);
	ASSERT_TRUE(0 == actual);

	char *empty = strdup("");
	actual = kstring_ltrim(empty, delim);
	ASSERT_TRUE(0 == actual);

	free(nospaces);
	free(allspaces);
	free(empty);
	free(leftwithspaces);
}

TEST(kstring, kstring_next_token_allocs){
	const char *delim = " ";
	char *orig_line = strdup("show test");
	char *line = orig_line;
	char *tok = kstring_next_token_allocs(&line, delim);

	ASSERT_TRUE(!strcmp("show", tok));
	ASSERT_TRUE(!strcmp("test", line));
	free(tok);
	free(orig_line);

	orig_line = strdup("");
	line = orig_line;
	tok = kstring_next_token_allocs(&line, delim);
	ASSERT_TRUE(0 == tok);
	free(orig_line);


}

TEST(kstring, kstring_split_count){
	char *line = strdup("    arg1 arg2    arg3 arg4     ");
	int argc = kstring_split_count(line, " ");

	//kstring_split_count does not remove empty values
	ASSERT_EQ(argc, 6);
	free(line);
}

TEST(kstring, kstring_split_allocs){
	char *line = strdup("    arg1 arg2    arg3 arg4     ");
	int argc = 0;
	char **argv = kstring_split_allocs(line, " ", &argc);

	ASSERT_EQ(argc, 4);
	ASSERT_TRUE(!strcmp("arg1", argv[0]));
	ASSERT_TRUE(!strcmp("arg2", argv[1]));
	ASSERT_TRUE(!strcmp("arg3", argv[2]));
	ASSERT_TRUE(!strcmp("arg4", argv[3]));
	free(line);
	for (int i = 0; i < argc; i++) {
		free(argv[i]);
	}
	free(argv);
}
