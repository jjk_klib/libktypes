#include <gtest/gtest.h>
#include <gmock/gmock.h>

using namespace testing;

extern "C" {
    #include "kencode.h"
    #include <string.h>
}



TEST(kencode, kencode_hex_value ){
	uint8_t actual;

	kencode_hex_value('0', &actual);
	ASSERT_EQ(actual, 0x00);
	kencode_hex_value('1', &actual);
	ASSERT_EQ(actual, 0x01);
	kencode_hex_value('a', &actual);
	ASSERT_EQ(actual, 0x0a);
	kencode_hex_value('F', &actual);
	ASSERT_EQ(actual, 0x0f);

}

TEST(kencode, kencode_hex_encode){
	uint8_t bytes[] = { 0x00, 0x01, 0x10, 0x11, 0xff };
	const size_t bytes_len = sizeof(bytes);
	char chars[bytes_len * 2 + 1] = { 0 };

	int error = kencode_hex_encode(bytes, sizeof(bytes), chars, sizeof(chars) - 1);

	ASSERT_EQ(0, error);
	ASSERT_EQ(0, strcmp(chars, "00011011ff"));

}


TEST(kencode, kencode_hex_decode){
	uint8_t bytes[5];
	const char chars[] = "00011011ff";

	int error = kencode_hex_decode(chars, strlen(chars), bytes, sizeof(bytes));

	ASSERT_EQ(0, error);
	uint8_t expected[] = { 0x00, 0x01, 0x10, 0x11, 0xff };
	ASSERT_EQ( memcmp(expected, bytes, sizeof(bytes)), 0);

	const char *not_hex = "ITISNOTHEX";
	error = kencode_hex_decode(not_hex, strlen(not_hex), bytes, sizeof(bytes));
	ASSERT_EQ(KENCODE_BAD_CHAR, error);

	const char *bad_len = "00011011f";
	error = kencode_hex_decode(bad_len, strlen(bad_len), bytes, sizeof(bytes));
	ASSERT_EQ(KENCODE_BAD_LENGTH, error);
}


